package br.ucsal.bes20222.testequalidade.locadora.mock;

import java.util.HashMap;
import java.util.Map;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20222.testequalidade.locadora.persistence.LocacaoDAO;

public class LocacaoDAOMock extends LocacaoDAO {

	// Map<nome-metodo-chamado, qtd-chamadas-feitas-para-este-metodo>
	private Map<String, Integer> chamadasMetodos = new HashMap<>();

	@Override
	public void insert(Locacao locacao) {
		chamadasMetodos.putIfAbsent("insert", 0);
		chamadasMetodos.put("insert", chamadasMetodos.get("insert") + 1);
	}

	public void verificarChamadasMetodos(String nomeMetodo, Integer qtdChamadasEsperadas) {
		if(!chamadasMetodos.containsKey(nomeMetodo)) {
			throw new RuntimeException("Este método nunca foi chamado para esta instância do mock.");
		}
		if(!chamadasMetodos.get(nomeMetodo).equals(qtdChamadasEsperadas)) {
			throw new RuntimeException("A quantidade de chamadas esperadas diferente da quantidade de chamadas feitas para o método.");
		}
	}
	
}
