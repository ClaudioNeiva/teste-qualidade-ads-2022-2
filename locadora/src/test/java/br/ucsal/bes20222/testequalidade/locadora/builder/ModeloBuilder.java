package br.ucsal.bes20222.testequalidade.locadora.builder;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Modelo;

public class ModeloBuilder {

	private static final String DEFAULT_NOME = "Gol";

	private String nome = DEFAULT_NOME;

	private ModeloBuilder() {
	}

	public static ModeloBuilder umModelo() {
		return new ModeloBuilder();
	}

	public ModeloBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public ModeloBuilder mas() {
		return new ModeloBuilder().comNome(nome);
	}
	
	public Modelo build() {
		return new Modelo(nome);
	}
}
