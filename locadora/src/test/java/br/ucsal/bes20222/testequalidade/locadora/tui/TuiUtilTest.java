package br.ucsal.bes20222.testequalidade.locadora.tui;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.bes20222.testequalidade.locadora.util.TuiUtil;

public class TuiUtilTest {

	@Test
	void testarObterNomeCompleto() {
		String nome = "claudio";
		String sobrenome = "neiva";

		String nomeCompletoEsperado = "claudio neiva";

		PrintStream printStreamOld = System.out;
		PrintStream printStreamMock = Mockito.mock(PrintStream.class);
		System.setOut(printStreamMock);
		
		Scanner scannerMock = Mockito.mock(Scanner.class);
		TuiUtil.scanner = scannerMock;
		Mockito.when(scannerMock.nextLine()).thenReturn(nome).thenReturn(sobrenome);

		String nomeCompletoAtual = TuiUtil.obterNomeCompleto();

		Assertions.assertAll(
			() -> Assertions.assertEquals(nomeCompletoEsperado, nomeCompletoAtual),
			() -> Mockito.verify(printStreamMock).println("Informe o nome:"),
			() -> Mockito.verify(printStreamMock).println("Informe o sobrenome:")
		);
		
		System.setOut(printStreamOld);
	}

}
