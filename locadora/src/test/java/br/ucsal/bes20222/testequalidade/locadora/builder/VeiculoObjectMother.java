package br.ucsal.bes20222.testequalidade.locadora.builder;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoObjectMother {

	public static Veiculo umVeiculoDisponivelBarato() {
		Veiculo veiculo = new Veiculo();
		String placa2 = "ABC-6780";
		veiculo.setPlaca(placa2);
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		veiculo.setValorDiaria(50.);
		veiculo.setAnoFabricacao(2015);
		veiculo.setModelo(new Modelo("Gol"));
		return veiculo;
	}

	public static Veiculo umVeiculoDisponivelCaro() {
		Veiculo veiculo = new Veiculo();
		String placa1 = "BCD-4567";
		veiculo.setPlaca(placa1);
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		veiculo.setValorDiaria(400.);
		veiculo.setAnoFabricacao(2015);
		veiculo.setModelo(new Modelo("Gol"));
		return veiculo;
	}

}
