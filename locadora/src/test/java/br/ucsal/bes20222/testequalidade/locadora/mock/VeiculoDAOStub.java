package br.ucsal.bes20222.testequalidade.locadora.mock;

import java.util.List;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.persistence.VeiculoDAO;

public class VeiculoDAOStub extends VeiculoDAO {

	private List<Veiculo> veiculosStub;

	public void setup(List<Veiculo> veiculosStub) {
		this.veiculosStub = veiculosStub;
	}

	@Override
	public List<Veiculo> obterPorPlacas(List<String> placas) throws VeiculoNaoEncontradoException {
		return veiculosStub;
	}

}
