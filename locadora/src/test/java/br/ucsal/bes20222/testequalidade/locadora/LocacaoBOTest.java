package br.ucsal.bes20222.testequalidade.locadora;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mock;
import org.mockito.Mockito;

import br.ucsal.bes20222.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20222.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.persistence.ClienteDAO;
import br.ucsal.bes20222.testequalidade.locadora.persistence.LocacaoDAO;
import br.ucsal.bes20222.testequalidade.locadora.persistence.VeiculoDAO;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
@TestInstance(Lifecycle.PER_CLASS)
public class LocacaoBOTest {

	@Mock
	private VeiculoDAO veiculoDAOMock;
	@Mock
	private ClienteDAO clienteDAOMock;
	@Mock
	private LocacaoDAO locacaoDAOMock;

	// @InjectMocks
	private LocacaoBO locacaoBOSpy;

	private Veiculo veiculo1;
	private Veiculo veiculo2;
	private Cliente cliente;

	@BeforeAll
	void setup() throws Exception {
		veiculoDAOMock = Mockito.mock(VeiculoDAO.class);
		clienteDAOMock = Mockito.mock(ClienteDAO.class);
		locacaoDAOMock = Mockito.mock(LocacaoDAO.class);
		// MockitoAnnotations.openMocks(this).close();
		locacaoBOSpy = Mockito.spy(new LocacaoBO(locacaoDAOMock, veiculoDAOMock, clienteDAOMock));
		// locacaoBOMock = Mockito.mock(LocacaoBO.class);
		// locacaoBOMock.veiculoDAO = veiculoDAOMock;
		// locacaoBOMock.clienteDAO = clienteDAOMock;
		// locacaoBOMock.locacaoDAO = locacaoDAOMock;

		criarVeiculosTest();
		criarClientesTest();
	}

	/**
	 * Testar o cálculo do valor total de locação por 4 dias de 2 veículos
	 * fabricados em 2015.
	 * 
	 * Caso de teste:
	 * 
	 * # 1
	 * 
	 * Entrada: 2 veículos fabricados em 2015
	 * 
	 * Entrada1: JPJ-1086, 2015, com valor da diária 100, modelo Gol, disponível
	 * 
	 * Entrada2: ABC-1234, 2015, com valor da diária 80, modelo Gol, disponível
	 * 
	 * Entrada3: ano de referência 2022
	 * 
	 * Saída: valor de locação esperado = 576
	 * 
	 * @throws VeiculoNaoEncontradoException
	 * 
	 */
	@Test
	public void testarCalculoValorTotalLocacao2Veiculos4Dias() throws VeiculoNaoEncontradoException {
		List<String> placas = Arrays.asList(veiculo1.getPlaca(), veiculo2.getPlaca());
		Integer qtdDiasLocacao = 4;
		LocalDate dataReferencia = LocalDate.of(2022, 10, 10);

		Mockito.when(veiculoDAOMock.obterPorPlacas(placas)).thenReturn(Arrays.asList(veiculo1, veiculo2));

		Double valorLocacaoEsperado = 576.;

		Double valorLocacaoAtual = locacaoBOSpy.calcularValorTotalLocacao(placas, qtdDiasLocacao, dataReferencia);

		Assertions.assertEquals(valorLocacaoEsperado, valorLocacaoAtual);
	}

	@Test
	void testarSalvarLocacaoValida() throws VeiculoNaoEncontradoException, ClienteNaoEncontradoException {
		List<String> placas = Arrays.asList(veiculo1.getPlaca(), veiculo2.getPlaca());
		Integer qtdDiasLocacao = 4;
		LocalDate dataLocacao = LocalDate.of(2022, 10, 10);
		Double valorLocacao = 576.;

		Mockito.when(veiculoDAOMock.obterPorPlacas(placas)).thenReturn(Arrays.asList(veiculo1, veiculo2));
		Mockito.when(clienteDAOMock.obterPorCpf(cliente.getCpf())).thenReturn(cliente);
		// Mockito.when(locacaoBOMock.calcularValorTotalLocacao(placas, qtdDiasLocacao,
		// dataLocacao))
		// .thenReturn(valorLocacao);
		Mockito.doReturn(valorLocacao).when(locacaoBOSpy).calcularValorTotalLocacao(placas, qtdDiasLocacao,
				dataLocacao);
		// Mockito.doCallRealMethod().when(locacaoBOSpy).salvar(cliente.getCpf(), placas, qtdDiasLocacao, dataLocacao);

		locacaoBOSpy.salvar(cliente.getCpf(), placas, qtdDiasLocacao, dataLocacao);

		Locacao locacaoEsperada = new Locacao(cliente, Arrays.asList(veiculo1, veiculo2), dataLocacao, qtdDiasLocacao,
				valorLocacao);
		Mockito.verify(locacaoDAOMock).insert(locacaoEsperada);
	}

	private void criarVeiculosTest() {
		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculoDisponivel().fabricadoEm(2015);
		veiculo1 = veiculoBuilder.mas().comPlaca("JPJ-1086").comValorDiaria(100.).build();
		veiculo2 = veiculoBuilder.mas().comPlaca("ABC-1234").comValorDiaria(80.).build();
		veiculoDAOMock.insert(veiculo1);
		veiculoDAOMock.insert(veiculo2);
	}

	private void criarClientesTest() {
		// TODO Criar ClienteBuilder.
		cliente = new Cliente("1234", "Claudio", "1234-21324");
		clienteDAOMock.insert(cliente);
	}

}
