package br.ucsal.bes20222.testequalidade.locadora;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import br.ucsal.bes20222.testequalidade.locadora.builder.VeiculoBuilder;
import br.ucsal.bes20222.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.mock.ClienteDAOStub;
import br.ucsal.bes20222.testequalidade.locadora.mock.LocacaoDAOMock;
import br.ucsal.bes20222.testequalidade.locadora.mock.VeiculoDAOStub;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
@TestInstance(Lifecycle.PER_CLASS)
public class LocacaoBOSemMockitoTest {

	private VeiculoDAOStub veiculoDAOStub;
	private ClienteDAOStub clienteDAOStub;
	private LocacaoDAOMock locacaoDAOMock;
	private LocacaoBO locacaoBO;

	private Veiculo veiculo1;
	private Veiculo veiculo2;
	private Cliente cliente;

	@BeforeAll
	void setup() {
		veiculoDAOStub = new VeiculoDAOStub();
		clienteDAOStub = new ClienteDAOStub();
		locacaoDAOMock = new LocacaoDAOMock();
		locacaoBO = new LocacaoBO(locacaoDAOMock, veiculoDAOStub, clienteDAOStub);
		criarVeiculosTest();
		criarClientesTest();
	}

	/**
	 * Testar o cálculo do valor total de locação por 4 dias de 2 veículos
	 * fabricados em 2015.
	 * 
	 * Caso de teste:
	 * 
	 * # 1
	 * 
	 * Entrada: 2 veículos fabricados em 2015
	 * 
	 * Entrada1: JPJ-1086, 2015, com valor da diária 100, modelo Gol, disponível
	 * 
	 * Entrada2: ABC-1234, 2015, com valor da diária 80, modelo Gol, disponível
	 * 
	 * Entrada3: ano de referência 2022
	 * 
	 * Saída: valor de locação esperado = 576
	 * 
	 * @throws VeiculoNaoEncontradoException
	 * 
	 */
	@Test
	public void testarCalculoValorTotalLocacao2Veiculos4Dias() throws VeiculoNaoEncontradoException {
		List<String> placas = Arrays.asList(veiculo1.getPlaca(), veiculo2.getPlaca());
		Integer qtdDiasLocacao = 4;
		LocalDate dataReferencia = LocalDate.of(2022, 10, 10);

		veiculoDAOStub.setup(Arrays.asList(veiculo1, veiculo2));

		Double valorLocacaoEsperado = 576.;

		Double valorLocacaoAtual = locacaoBO.calcularValorTotalLocacao(placas, qtdDiasLocacao, dataReferencia);

		Assertions.assertEquals(valorLocacaoEsperado, valorLocacaoAtual);
	}

	@Test
	void testarSalvarLocacaoValida() throws VeiculoNaoEncontradoException, ClienteNaoEncontradoException {
		List<String> placas = Arrays.asList(veiculo1.getPlaca(), veiculo2.getPlaca());
		Integer qtdDiasLocacao = 4;
		LocalDate dataLocacao = LocalDate.of(2022, 10, 10);

		veiculoDAOStub.setup(Arrays.asList(veiculo1, veiculo2));
		clienteDAOStub.setup(cliente);

		locacaoBO.salvar(cliente.getCpf(), placas, qtdDiasLocacao, dataLocacao);

		locacaoDAOMock.verificarChamadasMetodos("insert", 1);
		// Verificar não só a quantidade de chamadas, mas também para qual objeto de
		// locação a chamada foi feita.
	}

	private void criarVeiculosTest() {
		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculoDisponivel().fabricadoEm(2015);
		veiculo1 = veiculoBuilder.mas().comPlaca("JPJ-1086").comValorDiaria(100.).build();
		veiculo2 = veiculoBuilder.mas().comPlaca("ABC-1234").comValorDiaria(80.).build();
		veiculoDAOStub.insert(veiculo1);
		veiculoDAOStub.insert(veiculo2);
	}

	private void criarClientesTest() {
		// TODO Criar ClienteBuilder.
		cliente = new Cliente("1234", "Claudio", "1234-21324");
		clienteDAOStub.insert(cliente);
	}

}
