package br.ucsal.bes20222.testequalidade.locadora;

import java.lang.reflect.Method;

import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

public class Classe1Test {

	@Test
	public void testarMetodo4() throws Exception {
		Classe1 objeto1 = new Classe1();
		Classe1 spy1 = Mockito.spy(objeto1);

		Method metodo2 = Classe1.class.getDeclaredMethod("metodo2");
		metodo2.setAccessible(true);

		try (MockedStatic<Classe1> classe1Mock = Mockito.mockStatic(Classe1.class)) {

			classe1Mock.when(Classe1::metodo1).thenReturn(10);
			
			classe1Mock.when(() -> metodo2.invoke(classe1Mock)).thenReturn(20);
			// metodo3 ????

			spy1.metodo4();

			classe1Mock.verify(Classe1::metodo1);
			classe1Mock.verify(() -> metodo2.invoke(classe1Mock));
		}
	}

}
