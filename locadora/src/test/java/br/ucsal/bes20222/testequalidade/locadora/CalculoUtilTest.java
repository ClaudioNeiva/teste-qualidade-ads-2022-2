package br.ucsal.bes20222.testequalidade.locadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CalculoUtilTest {

	@ParameterizedTest
	@CsvSource({ "0,1", "1,1", "5,120" })
	void testarCalcularFatorial0(int n, Long fatorialEsperado) {
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
