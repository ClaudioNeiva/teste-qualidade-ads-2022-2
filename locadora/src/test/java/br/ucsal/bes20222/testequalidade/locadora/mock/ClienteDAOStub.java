package br.ucsal.bes20222.testequalidade.locadora.mock;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20222.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.persistence.ClienteDAO;

public class ClienteDAOStub extends ClienteDAO {

	private Cliente clienteEsperado;

	public void setup(Cliente clienteEsperado) {
		this.clienteEsperado = clienteEsperado;
	}

	@Override
	public Cliente obterPorCpf(String cpf) throws ClienteNaoEncontradoException {
		return clienteEsperado;
	}

}
