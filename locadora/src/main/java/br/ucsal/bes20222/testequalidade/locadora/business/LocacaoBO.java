package br.ucsal.bes20222.testequalidade.locadora.business;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20222.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20222.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20222.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20222.testequalidade.locadora.persistence.ClienteDAO;
import br.ucsal.bes20222.testequalidade.locadora.persistence.LocacaoDAO;
import br.ucsal.bes20222.testequalidade.locadora.persistence.VeiculoDAO;

public class LocacaoBO {

	private static final int QTD_ANOS_DESCONTO = 5;
	private static final double PERCENTUAL_DESCONTO = .2;

	private LocacaoDAO locacaoDAO;
	public VeiculoDAO veiculoDAO;
	private ClienteDAO clienteDAO;

	public LocacaoBO(LocacaoDAO locacaoDAO, VeiculoDAO veiculoDAO, ClienteDAO clienteDAO) {
		this.locacaoDAO = locacaoDAO;
		this.veiculoDAO = veiculoDAO;
		this.clienteDAO = clienteDAO;
	}

	/**
	 * Calcula o valor total da locaÃƒÂ§ÃƒÂ£o dos veÃƒÂ­culos para uma quantidade de
	 * dias. VeÃƒÂ­culos com mais de 5 anos de fabricaÃƒÂ§ÃƒÂ£o tÃƒÂªm desconto de
	 * 20%.
	 * 
	 * @param placas                placas dos veÃ­culos que serÃƒÂ£o locados
	 * @param quantidadeDiasLocacao quantidade de dias de locaÃƒÂ§ÃƒÂ£o
	 * @return
	 * @throws VeiculoNaoEncontradoException
	 */
	public Double calcularValorTotalLocacao(List<String> placas, Integer quantidadeDiasLocacao,
			LocalDate dataReferencia) throws VeiculoNaoEncontradoException {
		// System.out.println("entrei no calcularValorTotalLocacao...");
		Double total = 0d;
		Double valorLocacaoVeiculo;
		Integer anoAtual = dataReferencia.getYear();
		List<Veiculo> veiculos = veiculoDAO.obterPorPlacas(placas);

		for (Veiculo veiculo : veiculos) {
			valorLocacaoVeiculo = veiculo.getValorDiaria() * quantidadeDiasLocacao;
			if (veiculo.getAnoFabricacao() < anoAtual - QTD_ANOS_DESCONTO) {
				// FIXME Esse código está errado, para que o teste unitário passe o integrado
				// falhe. ( 1 -
				valorLocacaoVeiculo *= PERCENTUAL_DESCONTO;
			}
			total += valorLocacaoVeiculo;
		}

		return total;
	}

	public void salvar(String cpfCliente, List<String> placas, Integer quantidadeDiasLocacao, LocalDate dataLocacao)
			throws VeiculoNaoEncontradoException, ClienteNaoEncontradoException {
		List<Veiculo> veiculos = veiculoDAO.obterPorPlacas(placas);
		Double valorTotalLocacao = calcularValorTotalLocacao(placas, quantidadeDiasLocacao, dataLocacao);
		Cliente cliente = clienteDAO.obterPorCpf(cpfCliente);
		Locacao locacao = new Locacao(cliente, veiculos, dataLocacao, quantidadeDiasLocacao, valorTotalLocacao);
		locacaoDAO.insert(locacao);
	}

}
