package br.ucsal.bes20222.testequalidade.locadora;

public class CalculoUtil {

	public static Long calcularFatorial(int n) {
		Long fatorial = 1L;
		for (int i = 1; i <= n; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

}
