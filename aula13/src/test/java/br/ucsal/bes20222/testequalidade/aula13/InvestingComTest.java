package br.ucsal.bes20222.testequalidade.aula13;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@TestInstance(Lifecycle.PER_CLASS)
public abstract class InvestingComTest {

	protected WebDriver driver;
	protected WebDriverWait wait;

	@AfterAll
	public void teardown() {
		driver.quit();
	}

	@Test
	void testarPesquisa() throws InterruptedException {
		
		// Abrir p�gina do Investing.com
		driver.get("http://br.investing.com");
		System.out.println("*************************************************************************************************************************");

		// P�ginas html dentro do projeto
		// driver.get(String.valueOf(InvestingComTest.class.getResource("/webapp/imposto.html")));
		
		// Fechar o bot�o de confirma��o de pol�tica de privacidade
		wait.until(ExpectedConditions.numberOfElementsToBe(By.id("onetrust-close-btn-handler"), 1));
		
		WebElement onetrustButton = driver.findElement(By.className("onetrust-close-btn-handler"));
		onetrustButton.click();

		// Preencher o input de "Pesquisar no site..."
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys("COGN3" + Keys.ENTER);

		// Obter o conte�do da p�gina
		// Thread.sleep(5000); // WebDriverWait?
		wait.until(ExpectedConditions.numberOfElementsToBe(By.id("js-inner-all-results-quotes-wrapper"), 1));
		String conteudo = driver.getPageSource();
		
		// Verificar se retorno inclui "Cogna Educa��o"
		Assertions.assertTrue(conteudo.contains("Cogna Educa��o"));
	}

}
