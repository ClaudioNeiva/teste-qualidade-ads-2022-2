package br.ucsal.bes20222.testequalidade.aula13;

import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class InvestingComFirefoxTest extends InvestingComTest {

	@BeforeAll
	void setup() {
		WebDriverManager.firefoxdriver().setup();
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 5);
	}

}
