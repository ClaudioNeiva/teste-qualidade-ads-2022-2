package br.ucsal.bes20222.testequalidade.aula12.aula12;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import br.ucsal.bes20222.testequalidade.aula12.Aluno;

public class LancamentoNotaSteps {

	private Aluno aluno;

	// Dado que um aluno está matriculado na disciplina
	@Given("um aluno está matriculado na disciplina")
	public void instanciarAluno() {
		aluno = new Aluno();
	}

	// Quando informo a nota *
	@When("informo a nota $nota")
	public void informarNota(Double nota) {
		aluno.informarNota(nota);
	}

	// Então a situação do aluno é *
	@Then("a situação do aluno é $situacao")
	public void verificarSituacaoAluno(String situacaoEsperada) {
		String situacaoAtual = aluno.obterSituacao();
		Assert.assertEquals(situacaoEsperada, situacaoAtual);
	}

}
