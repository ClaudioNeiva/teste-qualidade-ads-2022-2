package br.ucsal._20202.testequalidade.aula03;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

class PersitenteLinkedListTest {

	private static String tabName = "tab_listas";
	private static Connection connection;
	private static Long idLista = 100L;

	@BeforeAll
	static void setupClass() throws SQLException {
		// Pré-condição para execução do teste
		Assumptions.assumeTrue(DbUtil.isConnectionValid());
		connection = DbUtil.getConnection();
	}

	@BeforeEach
	void setup() throws SQLException {
		limparListaDataBase();
	}

	@AfterAll
	static void tearDownClass() throws SQLException {
		limparListaDataBase();
		// Se o utilitário DbUtil tiver um método de descontar, utilizar aqui!
	}

	@Test
	void testarPersistenciaLoad3Nomes()
			throws InvalidElementException, SQLException, IOException, ClassNotFoundException {
		// Dados de entrada - a lista com os 3 nomes.
		String nome0 = "antonio";
		String nome1 = "claudio";
		String nome2 = "neiva";
		String nomeDescartar = "pedreira";
		PersistentList<String> persistentList = new PersitenteLinkedList<>();
		persistentList.add(nome0);
		persistentList.add(nome1);
		persistentList.add(nome2);

		// Saída esperada - os elementos nome0, nome1 e nome2, nesta ordem e a lista com
		// 3 elementos
		int tamanhoListaEsperado = 3;

		// Executar o método persist, que é um dos objetivos do teste
		persistentList.persist(idLista, connection, tabName);

		// Obter o resultado atual e testar, implicitamente o método load
		PersistentList<String> persistentListAtual = new PersitenteLinkedList<>();
		persistentListAtual.add(nomeDescartar);
		persistentListAtual.load(idLista, connection, tabName);
		Assertions.assertAll(() -> Assertions.assertEquals(nome0, persistentListAtual.get(0), "primeiro nome"),
				() -> Assertions.assertEquals(nome1, persistentListAtual.get(1), "segundo nome"),
				() -> Assertions.assertEquals(nome2, persistentListAtual.get(2), "terceiro nome"),
				() -> Assertions.assertEquals(tamanhoListaEsperado, persistentListAtual.size(), "tamanho da lista"));
	}

	void testarPersistencia3Nomes() {
		// Ferrramenta de SGBD para verificar se o database no estado desejado.
	}

	void testarLoad3Nomes() {
		// Ferrramenta de SGBD para deixar o database no estado necessário para a carga.
	}

	private static void limparListaDataBase() throws SQLException {
		new PersitenteLinkedList<>().delete(idLista, connection, tabName);
	}

}
