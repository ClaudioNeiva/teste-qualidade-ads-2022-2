package br.ucsal._20202.testequalidade.aula03.util;

import java.time.LocalDate;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

@TestInstance(Lifecycle.PER_CLASS)
class CalculoUtilParametrizedTest {

	private CalculoUtil calculoUtil;

	@BeforeAll
	void setupClass() {
		calculoUtil = new CalculoUtil();
	}

	@ParameterizedTest(name = "{index} calcularFatorial({0}) ")
	@CsvSource(value = { "0:1", "1:1", "4:24", "5:120" }, delimiter = ':')
	void testarFatoriais(int n, long fatorialEsperado) {
		long fatorialAtual = calculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@ParameterizedTest(name = "{index} calcularFatorial({0}) ")
	@MethodSource("fornecerDadosTest")
	void testarFatoriais2(int n, long fatorialEsperado) {
		long fatorialAtual = calculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}
	
	private static Stream<Arguments> fornecerDadosTest() {
//		Aluno aluno1 = new Aluno(123,"claudio",LocalDate.of(2000, 10, 12));
//		Professor professor1 = new Professor(467567,"234234","João");
//		Disciplina disciplina1 = new Disciplina("BES008","Programação orientada a objetos") 
	    return Stream.of(
//	      Arguments.of(aluno1, professor1, disciplna1),
	    	Arguments.of(0, 1),
	    	Arguments.of(1, 1),
	  	    Arguments.of(4, 24),
	  	    Arguments.of(5, 120)
	    );
	}


}
