package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

@TestInstance(Lifecycle.PER_CLASS)
class LinkedListTest {

	private List<String> nomes;
	private String nome0;
	private String nome1;
	private String nome2;

	@BeforeAll
	void setupClass() {
		nome0 = "antonio";
		nome1 = "claudio";
		nome2 = "neiva";
	}
	
	@BeforeEach
	void setup() {
		nomes = new LinkedList<>();
	}

	@Test
	void testarAddGet3Nomes() throws InvalidElementException {
		// Executar o método que desejo testar
		nomes.add(nome0);
		nomes.add(nome1);
		nomes.add(nome2);

		// Obter o resultado e comparar o resultado esperado
		Assertions.assertAll(
				() -> Assertions.assertEquals(nome0, nomes.get(0), "primeiro nome"),
				() -> Assertions.assertEquals(nome1, nomes.get(1), "segundo nome"),
				() -> Assertions.assertEquals(nome2, nomes.get(2), "terceiro nome"));
		
	}
	
	@Test
	void testarSize2() throws InvalidElementException {
		int qtdEsperada = 2;
		nomes.add(nome0);
		nomes.add(nome1);
		int qtdAtual  = nomes.size();
		Assertions.assertEquals(qtdEsperada, qtdAtual);
	}
	
	@Test
	void testarAddNomeInvalido() {
		// Dados entrada
		String nomeInvalido = null;
		// Saída esperada (além da ocorrência da exceção)
		String mensagemEsperada = "O elemento não pode ser nulo.";
		// Executando o método a ser testado e obtendo o resultado atual e verificando
		InvalidElementException exceptionAtual = Assertions.assertThrows(InvalidElementException.class, () -> nomes.add(nomeInvalido));
		Assertions.assertEquals(mensagemEsperada, exceptionAtual.getMessage());
	}

	@Test
	void testarGetListaVazia() {
		Assertions.assertNull(nomes.get(0));
	}
	
}
