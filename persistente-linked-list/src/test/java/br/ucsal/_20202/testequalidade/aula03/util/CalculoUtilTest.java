package br.ucsal._20202.testequalidade.aula03.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
class CalculoUtilTest {

	private CalculoUtil calculoUtil;

	@BeforeAll
	void setupClass() {
		System.out.println("setupClass...");
	}

	@AfterAll
	void teardownClass() {
		System.out.println("teardownClass...");
	}

	@BeforeEach
	void setup() {
		System.out.println("setup...");
		calculoUtil = new CalculoUtil();
	}

	@AfterEach
	void teardown() {
		System.out.println("teardown...");
		calculoUtil = new CalculoUtil();
	}

	@Test
	@DisplayName("Calcular fatorial de 6")
	void testarCalcularFatorial6() {
		System.out.println("testarCalcularFatorial6...");
		// Definir dados de entrada
		int n = 6;
		// Definir o resultado esperado
		long fatorialEsperado = 720;
		// Executar o método que está sendo testado e obter o resultado atual
		long fatorialAtual = calculoUtil.calcularFatorial(n);
		// Compararo resultado esperado com o resultado atual
		assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	@Disabled
	void testarCalcularFatorial0() {
		System.out.println("testarCalcularFatorial0...");
		// Definir dados de entrada
		int n = 0;
		// Definir o resultado esperado
		long fatorialEsperado = 1;
		// Executar o método que está sendo testado e obter o resultado atual
		long fatorialAtual = calculoUtil.calcularFatorial(n);
		// Compararo resultado esperado com o resultado atual
		assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	void testarCalcularFatorial1() {
		System.out.println("testarCalcularFatorial1...");
		// Definir dados de entrada
		int n = 1;
		// Definir o resultado esperado
		long fatorialEsperado = 1;
		// Executar o método que está sendo testado e obter o resultado atual
		long fatorialAtual = calculoUtil.calcularFatorial(n);
		// Compararo resultado esperado com o resultado atual
		assertEquals(fatorialEsperado, fatorialAtual);
	}
	
}
